import java.util.Scanner;

public class Shop {
  public static void main(String args[]){
    Shoe[] shoeShelf = new Shoe[4];
	
    //Initiate Scanner
    Scanner keyboard = new Scanner(System.in);
    
	//Initiate Variables
    String brand;
    double size;
    String color;
    
    //Initatite string to skip a line to skip a line
    String skip;
    
    //Create Loop to get input from user for each pair of shoes in the shoe shelf
    for (int i = 0; i < shoeShelf.length; i++){
      int number = i+1;
      
      //Get the values with a scanner
      System.out.print("Enter the brand for shoe #"+ number +": ");
      brand = keyboard.nextLine();
      System.out.print("Enter the color for shoe #"+ number +": ");
      color = keyboard.nextLine();
      System.out.print("Enter the size for shoe #"+ number +": ");
      size = keyboard.nextDouble();
      
      //This is to avoid a bug where it skips a line
      skip = keyboard.nextLine();
      
      // Store the values in the array
      shoeShelf[i] = new Shoe(brand, size, color);
    }
	
    //Print the characteristics of the last pair of shoes on the shelf
    System.out.println(shoeShelf[3].getBrand() +", "+ shoeShelf[3].getSize() +", "+ shoeShelf[3].getColor() +".");
    
    //Describe the shoes 
    shoeShelf[3].describeShoes();
	
	//Get new values for the last pair of shoes
	System.out.print("Enter a new brand for shoe #"+ 4 +": ");
    brand = keyboard.nextLine();
	System.out.print("Enter a new color for shoe #"+ 4 +": ");
    color = keyboard.nextLine();
	
	shoeShelf[3].setBrand(brand);
	shoeShelf[3].setColor(color);
	
	//Print the characteristics of the last pair of shoes on the shelf
    System.out.println(shoeShelf[3].getBrand() +", "+ shoeShelf[3].getSize() +", "+ shoeShelf[3].getColor() +".");
    
    //Describe the shoes 
    shoeShelf[3].describeShoes();
  }
}