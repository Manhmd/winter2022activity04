public class Shoe {
  private String brand;
  private double size;
  private String color;
  
  public void describeShoes(){
   System.out.println("These are "+ this.color +", "+ this.brand +" brand shoes that have a size of "+ this.size +".");
  }
  
  public Shoe(String initBrand, double initSize, String initColor){
    this.brand = initBrand;
    
    this.size = initSize;
    
    //check if the size is a valid input
    if (this.size < 0){
      this.size = 0;
      System.out.println("This size input is invalid");
    }
    
    this.color = initColor;
  }
  
  public void setBrand(String brand){
   this.brand = brand;
  }
  
  public void setSize(double size){
   this.size = size;
  }
  
  public void setColor(String color){
   this.color = color;
  }
  
  public String getBrand(){
   return this.brand;
  }
  
  public double getSize(){
   return this.size;
  }
  
  public String getColor(){
   return this.color;
  }
}